#!/bin/bash
#
for i1 in \
scour \
; do
  sbopkg -B -i $i1 || exit
done

for i2 in \
pstoedit \
; do 
  cd source/$i2 || exit 1
  sh $i2.SlackBuild || exit 1
  upgradepkg --install-new --reinstall /tmp/$i2*_SBo.t?z || exit 1 
  cd ../..
done

src=(
potrace
python2-numpy
python3-webencodings
html5lib
python-toml
python2-setuptools-scm
functools-lru-cache
python2-soupsieve
python2-BeautifulSoup4
python3-soupsieve
BeautifulSoup4
lxml
libcdr
double-conversion
dos2unix
gdl
)

for i3 in ${src[@]}; do
	sbopkg -B -i $i3 || exit 1
done

for i4 in \
GraphicsMagick \
kelbt \
colm \
ragel \
gtest \
lib2geom \
inkscape \
; do
 cd source/$i4 || exit 1
 sh $i4.SlackBuild || exit 1
  upgradepkg --install-new --reinstall /tmp/$i4*_SBo.t?z || exit 1 
 cd ../..
done

exit 0
