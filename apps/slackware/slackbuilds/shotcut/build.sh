#!/bin/bash

for part1 in \
ladspa_sdk \
x264 \
x265 \
libfdk-aac \
jack \
; do
cd ${part1} || exit 1
./${part1}.SlackBuild || exit 1
cd ..
done

for part2 in \
	ffmpeg \
	; do
cd ${part2} || exit 1
FDK_AAC=yes LAME=yes X264=yes X265=yes ./${part2}.SlackBuild || exit 1
cd ..
done

for part3 in \
	mlt \
	; do
cd ${part3} || exit 1
./${part3}.SlackBuild || exit 1
cd ..
done

for part4 in \
	shotcut \
	; do
cd ${part4} || exit 1
./${part4}.SlackBuild || exit 1
cd ..
done
