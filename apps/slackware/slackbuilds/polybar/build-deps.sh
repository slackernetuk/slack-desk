#!/bin/bash

for i in \
sphinxcontrib-serializinghtml \
sphinxcontrib-qthelp \
sphinxcontrib-jsmath \
sphinxcontrib-htmlhelp \
sphinxcontrib-devhelp \
sphinxcontrib-applehelp \
snowballstemmer \
MarkupSafe \
Jinja2 \
pytz \
python3-babel \
imagesize \
alabaster \
Sphinx \
libmpdclient \
jsoncpp \
siji-font \
xcb-util-xrm \
gnu-unifont \
; do
sbopkg -B -i $i || exit 1
done
