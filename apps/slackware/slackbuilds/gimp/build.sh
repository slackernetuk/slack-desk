#!/bin/bash

for i in \
babl \
chrpath \
OpenBLAS \
blas \
lapack \
suitesparse \
maxflow \
gegl \
luajit \
appstream-glib \
gimp \
GraphicsMagick \
gmic-gimp \
; do
cd "${i}" || exit 1
./"${i}".SlackBuild || exit 1
cd ..
done
